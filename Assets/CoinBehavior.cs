﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinBehavior : MonoBehaviour
{
    public int value = 1;
    public GameObject ui;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        ui = GameObject.FindGameObjectWithTag("CoinAmount");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var coinsUI = int.Parse(ui.GetComponent<Text>().text) + value;
            ui.GetComponent<Text>().text = coinsUI + "";
            animator.SetBool("IsCollect", true);
        
        }
    }
}
